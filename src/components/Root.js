import React, { Component } from 'react';
import Header from './header/index';
import Footer from './footer/index';
import Home from './home/index'
import About from './about/index';
import Gallery from './gallery/index';
import NotFound from './redirect/404';
import UnderConstruction from './redirect/UnderConstruction';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

export class Root extends Component {
  render() {
    return (
      <Router>
        <div>

          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={About} />
            <Route exact path="/gallery" component={Gallery} />
            <Route exact path="/work" component={UnderConstruction} />
            <Route path="*" component={NotFound} />
          </Switch>
          <div class="container back-to-top top-venti" id="move-to-top">Back to Top</div>
          <Footer />

        </div>
      </Router>
    )
  }
}

export default Root
