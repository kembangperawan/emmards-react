import React, { Component } from 'react';
import Galleries from '../gallery/Galleries';
import axios from 'axios';
import './style.css';

export class Home extends Component {
  constructor () {
    super();
    this.state = {
      galleries: []
    }
  }

  componentDidMount() {
      axios.get(`http://localhost/snhadmin/api/image_gallery/list?limit=8`)
      .then(res => {
        const image_galleries = res.data.image_galleries.data;
        this.setState({ galleries: image_galleries });
      });
  }
  render() {
    return (
      <div>
        <div className="container">
          <div className="row gallery">
            <div className="col-md-4 col-sm-6 col-xs-12">
                <div className="text-center"><b>Hi</b>, welcome to my website;</div>
                <div className="profile">
                    <img src="./images/thumbnail.JPG" alt="profile" title="emmanuel" />
                    <div className="name">I'm emmanuel</div>
                    <div className="description">An ordinary person who want to speak 5 languages fluently.</div>
                    <br />
                    <div className="social">
                        <ul className="list-inline">
                            <li><a href="mailto:surabi.eman@gmail.com" rel="noopener noreferrer"><i className="far fa-envelope"></i></a></li>
                            <li><a className="instagram" href="https://www.instagram.com/emmards/" rel="noopener noreferrer" target="_blank"><i className="fab fa-instagram"></i></a></li>
                            <li><a className="twitter" href="https://twitter.com/emmards" rel="noopener noreferrer" target="_blank"><i className="fab fa-twitter"></i></a></li>
                            <li><a className="linkedin" href="https://www.linkedin.com/in/emmanuel-ardianto-703b1a67/" rel="noopener noreferrer" target="_blank"><i className="fab fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <Galleries galleries = { this.state.galleries } />
          </div>
        </div>
      </div>
    )
  }
}

export default Home
