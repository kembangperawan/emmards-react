import React, { Component } from 'react'

export class NotFound extends Component {
    render() {
        return (
            <div className="text-center top-venti">
                <h2>Oops!</h2>
                <div className="bottom-grande">ごめんなさい！</div>
                <div>We don't have what you are looking for.</div>
                <div className="bottom-grande">そのページはありません。</div>
                <div className="bottom-venti">
                    <img src="./images/sorry.gif" title="under construction" alt="under construction" style={{ width: '300px' }} />
                    <div>gif taken from <a href="https://tenor.com/view/ppkk08-sorry-adventure-time-jake-gif-8025376" rel="noopener noreferrer" target="_blank">tenor</a></div>
                </div>
                <div>404 not found.</div>
            </div>
        )
    }
}

export default NotFound
