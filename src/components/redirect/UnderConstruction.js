import React, { Component } from 'react'

export class UnderConstruction extends Component {
    render() {
        return (
            <div class="text-center top-venti">
                <div>We're working on it.</div>
                <div class="bottom-grande">我々はそれに取り組んでいます。待ってね！</div>
                <div class="bottom-venti">
                    <img src="./images/under-construction.gif" title="under construction" alt="under construction" style={{ width: '400px' }} />
                    <div>gif taken from <a href="https://giphy.com/gifs/adventure-time-cooking-bacon-kkQfnuqlSifgQ" rel="noopener noreferrer" target="_blank">giphy</a></div>
                </div>
                <div>Under construction.</div>
            </div>
        )
    }
}

export default UnderConstruction
