import React, { Component } from 'react'

export class Skill extends Component {
    render() {
        return (
            <div className="col">
                <div className="card bottom-venti">
                    <div className="card-body">
                        <h5 className="card-title">{this.props.data.title}</h5>
                        <div className="card-text">
                            <ul>
                                {this.props.data.items.map((item, i) => <li key={i}>{item}</li>)}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Skill
