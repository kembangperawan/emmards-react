import React, { Component } from 'react'

export class Positions extends Component {
    render() {
        return (
            <div className="col">
                <div className="card bottom-venti">
                    <div className="card-body">
                        <h5 className="card-title">{this.props.data.position}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{this.props.data.date}</h6>
                        <div className="card-text">
                            {this.props.data.responsibilities}
                            <br/>
                            <strong>Projetc(s) :</strong>
                            <ul>
                                { this.props.data.project.map((item, i) => <li key={ i }>{item}</li>)}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Positions
