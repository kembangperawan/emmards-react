import React, { Component } from 'react'

export class Other extends Component {
  render() {
    return (
      <div class="bottom-tall">
        <strong>{ this.props.data.key } : </strong> { this.props.data.value }
      </div>
    )
  }
}

export default Other
