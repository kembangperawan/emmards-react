import React, { Component } from 'react'

export class Education extends Component {
    render() {
        return (
            <div className="col">
                <div className="card bottom-venti">
                    <div className="card-body">
                        <h5 className="card-title">{this.props.data.institution}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{this.props.data.date}</h6>
                        <div className="card-text">
                            <div><strong>Grade : </strong> { this.props.data.grade }</div>
                            <div><strong>Field of study : </strong> { this.props.data.field }</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Education
