import React, { Component } from 'react';
import Position from './Positions';

export class WorkExperience extends Component {
  render() {
    return (
        <div>
            <h3>{ this.props.data.employer }</h3>
            <p>{ this.props.data.type_of_business }</p>
            <div className="row">
            {
                this.props.data.positions.map((item, i) => {
                    return <Position key={i} data={item} />
                })
            }
            </div>
        </div>
    )
  }
}

export default WorkExperience
