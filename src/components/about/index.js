import React, { Component } from 'react';
import axios from 'axios';
import WorkExperience from './WorkExperience';
import Education from './Education';
import Skill from './Skill';
import Other from './Other';
import './style.css';

export class About extends Component {
  constructor() {
    super();
    this.state = {
      work_experience: [],
      introduction: '',
      education: [],
      skill: [],
      other: []
    }

  }
  componentWillMount() {

  }
  componentDidMount() {
    this.loadData();
  }
  loadData() {
    axios.get(`http://localhost/snhadmin/api/about`)
      .then(res => {
        const about = res.data.about;
        this.setState({
          introduction: about.introduction.en,
          work_experience: about.work_experience,
          education: about.education.en,
          skill: about.skill,
          other: about.other
        });
      });
  }
  render() {
    if (!this.state.work_experience) {
      return <div />
    }
    console.log(this.state.about);
    return (
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="bottom-venti">
              <h1>About Me</h1>
              <p>You got me! Here I am;</p>
            </div>
            <div className="bottom-tall">
              <img src="./images/about-me-banner.JPG" title="this is me" alt="this is me" />
            </div>
            <div dangerouslySetInnerHTML={{__html: this.state.introduction}}></div>
            <h2 className="bottom-grande">Work Experiences</h2>
            <div>
              {
                this.state.work_experience.map((item, i) => {
                  return <WorkExperience key={i} data={item} />
                })
              }
            </div>
            <h2 className="bottom-grande">Education</h2>
            <div className="row">
              {
                this.state.education.map((item, i) => {
                  return <Education key={i} data={item} />
                })
              }
            </div>
            <h2 className="bottom-grande">Skill</h2>
            <div className="row">
              {
                this.state.skill.map((item, i) => {
                  return <Skill key={i} data={item} />
                })
              }
            </div>
            <h2 className="bottom-grande">Other</h2>
              {
                this.state.other.map((item, i) => {
                  return <Other key={i} data={item} />
                })
              }
          </div>
        </div>
      </div>
    )
  }
}

export default About
