import React, { Component } from 'react'

export class GalleryItem extends Component {
  render() {
    return (
        <div className="col-md-4 col-sm-6 col-xs-12 item">
            <a className="img-thumbnail" href="/">
                <img src={ this.props.gallery.image_url } alt={ this.props.gallery.caption } title={ this.props.gallery.caption } className={ this.props.gallery.orientation === 'PORTRAIT' ? 'portrait' : '' } />
            </a>
            <p className="caption" dangerouslySetInnerHTML={{__html: this.props.gallery.caption}}></p>
        </div>
    )
  }
}

export default GalleryItem
