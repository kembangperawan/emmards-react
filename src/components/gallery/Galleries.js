import React, { Component } from 'react'
import GalleryItem from './GalleryItem';
import './style.css';

export class Galleries extends Component {
  render() {
    return (
        this.props.galleries.map((gallery, i) => {
            return <GalleryItem key={gallery.id}  gallery={ gallery } />
        })
    )
  }
}

export default Galleries
