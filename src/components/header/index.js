import React, { Component } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import './style.css';

export class Header extends Component {
    constructor() {
        super();
        this.state = {
            scrolled: false
        }
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        if (window.scrollY > 5) {
            this.setState({
                scrolled: true
            });
        } else {
            this.setState({
                scrolled: false
            });
        }
    }
    render() {
        return (
            <Navbar expand="lg" fixed="top" className={"navbar navbar-default" + (this.state.scrolled ? "" : " no-border")}>
                <Container>
                    <Navbar.Brand href="/">EMMARDS</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ml-auto main-menu">
                            <Nav.Link href="/about">About Me</Nav.Link>
                            <Nav.Link href="/work">Works</Nav.Link>
                            <Nav.Link href="/gallery">Galleries</Nav.Link>
                            <Nav.Link href="/story">Stories</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        )
    }
}

export default Header
