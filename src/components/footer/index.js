import React, { Component } from 'react';
import './style.css';

export class Footer extends Component {
  render() {
    return (
      <footer>
         <div className="container footer">
            <div className="social">
                <ul className="list-inline">
                    <li><a href="mailto:surabi.eman@gmail.com" rel="nofollow"><i className="far fa-envelope"></i></a></li>
                    <li><a className="instagram" href="https://www.instagram.com/emmards/" rel="noopener noreferrer" target="_blank"><i className="fab fa-instagram"></i></a></li>
                    <li><a className="twitter" href="https://www.instagram.com/emmards/" rel="noopener noreferrer" target="_blank"><i className="fab fa-twitter"></i></a></li>
                    <li><a className="linkedin" href="https://www.instagram.com/emmards/" rel="noopener noreferrer" target="_blank"><i className="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div>CSS by <a href="https://getbootstrap.com/" rel="noopener noreferrer" target="_blank">Bootstrap</a>, Icon by <a href="https://fontawesome.com/" rel="noopener noreferrer" target="_blank">Font Awesome</a>, Font by <a href="https://fonts.google.com/specimen/Dosis" rel="noopener noreferrer" target="_blank">Dosis</a></div>
            <div className="copyright">&copy; <a href="/">emmards</a> 2018</div>
        </div>
      </footer>
    )
  }
}

export default Footer
